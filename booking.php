<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <link href="css/base.css" rel="stylesheet"/>
        <link href="css/booking.css" rel="stylesheet" />
        <title>Les Happy hours</title>
    </head>

    <body>
        <header>
            <h1>
                <strong><a href="index.php">Les Happy hours</a></strong>
            </h1>
            <p>Troupe d'improvisation théâtrale de Grenoble</p>
            <p>
                <img alt="Photo de la troupe en représentation" src="images/header.jpg" />
            </p>
        </header>

        <nav>
            <ul>
                <li><a href="index.php">Accueil</a></li>
                <li><a href="improvisation_troupe.php">La troupe</a></li>
                <li><a href="our_shows.php">Nos spectacles</a></li>
                <li><a href="calendar.php">Calendrier</a></li>
                <li><a href="booking.php">Réservations</a></li>
                <li><a href="contact.php">Contact</a></li>
                <li><a href="admin/admin.php">administrateur</a></li>
            </ul>
        </nav>

        <section>
            <p>
                <a href="index.php">Accueil</a> > Réservations
            </p><br>
            <h1>Réservations</h1><br>
            <form action="traitement.php" method="post"> <!-- pour l'instant, récuppération des données n'est pas faite -->
                <h1> Réservation pour les spectacles des Happy hours</h1>
                <p> Vos tickets réservés seront à récupérer et à régler sur place le soir du spectacle.<p>
                <p> *Obligatoire </p>
                <p>
                    <label for="firstName">Prénom</label><br>
                    <input id="firstName" name="firstName" placeholder="Votre réponse" required type="text" />
                </p>
                <p>
                    <label for="lastName">Nom</label><br>
                    <input id="lastName" name="lastName" placeholder="Votre réponse" required type="text" />
                </p>
            </form>
        </section>

        <footer>
            <h5>Les Happy hours, troupe d'improvisation</h5>
            <div class="bottomFooter">
                <a href="index.php">Accueil</a> /
                <a href="improvisation_troupe.php">La troupe</a> /
                <a href="our_shows.php">Nos spectacles</a> /
                <a href="calendar.php">Calendrier</a> /
                <a href="booking.php">Réservations</a> /
                <a href="contact.php">Contact</a> /
                <a href="admin/admin.php">administrateur</a>
            </div>
        </footer>
    </body>
</html>