<!DOCTYPE html>
<html lang="fr" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="utf-8" />
        <link href="css/base.css" rel="stylesheet"/>
        <link href="css/contact.css" rel="stylesheet" />
        <title>Les Happy hours</title>
    </head>

    <body>
        <header>
            <h1>
                <strong><a href="index.php">Les Happy hours</a></strong>
            </h1>
            <p>Troupe d'improvisation théâtrale de Grenoble</p>
            <p>
                <img alt="Photo de la troupe en représentation" src="images/header.jpg" />
            </p>
        </header>

        <nav>
            <ul>
                <li><a href="index.php">Accueil</a></li>
                <li><a href="improvisation_troupe.php">La troupe</a></li>
                <li><a href="our_shows.php">Nos spectacles</a></li>
                <li><a href="calendar.php">Calendrier</a></li>
                <li><a href="booking.php">Réservations</a></li>
                <li><a href="contact.php">Contact</a></li>
                <li><a href="admin/admin.php">administrateur</a></li>
            </ul>
        </nav>

        <section>
            <p>
                <a href="index.php">Accueil</a> > Contact
            </p><br>
            <h1>Contactez-nous</h1><br>
            <h3>Les Happy hours</h3>
            <p>...</p><br> <!-- adresse de la troupe (association) -->
            <form action="traitement_contact.php" method="post" id="contact"> <!-- pour l'instant, récuppération des données n'est pas faite -->
                <p>
                    <label for="lastName">Votre nom</label><br>
                    <input id="lastName" name="lastName" placeholder="Nom" size="30" type="text" />
                </p>
                <p>
                    <label for="email">Votre email</label><br>
                    <input id="email" name="email" placeholder="email" size="30" type="email" />
                </p>
                <p>
                    <label for="sujet">Sujet</label><br>
                    <input id="sujet" name="sujet" placeholder="Sujet" size="30" type="text" />
                </p>
                <p>
                    <label for="message">Votre message</label><br/>
                    <textarea cols="29" id="message" name="message" placeholder="Tapez ici votre message" rows="15"></textarea>
                </p>
                <p>
                    <!--<input id="lecture" name="lecture" required type="checkbox"/>
                    <label for="lecture">J'ai bien vérifié les informations avant l'envoie</label><br>-->
                    <button type="submit" name="envoi" value="Envoyer le message" </button>
                </p>
            </form><br>
            <p>Suivez nous !</p>
        </section>

        <footer>
            <h5> Les Happy hours, troupe d'improvisation</h5>
            <div class="bottomFooter">
                <a href="index.php">Accueil</a> /
                <a href="improvisation_troupe.php">La troupe</a> /
                <a href="our_shows.php">Nos spectacles</a> /
                <a href="calendar.php">Calendrier</a> /
                <a href="booking.php">Réservations</a> /
                <a href="contact.php">Contact</a> /
                <a href="admin/admin.php">administrateur</a>
            </div>
        </footer>
    </body>
</html>