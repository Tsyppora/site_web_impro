site_web_impro
---

Creation of a dynamic website for an improvisation troupe in Grenoble.
- Creation of a contact page with answer by email
- creation of a booking page with confirmation by email
- creation of an administrator interface with login page, logout
so that the leaders of the troop can make changes on
some pages of the site (informations on troop members, events
to come up, ...)

---
