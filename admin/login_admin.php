<?php


$bdd = new PDO('mysql:host=localhost;dbname=les_happy_hours;charset=utf8', 'root', '');
$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$selectLoginPassword = $bdd->prepare('SELECT login, password FROM administrateur');
$selectLoginPassword->execute();

$administrateur = [];

while ($donnees = $selectLoginPassword->fetch()){
    array_push($administrateur,$donnees);
}

$selectLoginPassword->closeCursor();

//print_r($administrateur[0]['password']);


try {
    if(isset($_POST['login']) && isset($_POST['password'])) {
        if ($_POST['login'] === $administrateur[0]['login'] && $_POST['password'] === $administrateur[0]['password']) {
            echo "vos identifiants sont corrects";
        } else {
            echo "vos identifiants sont incorrects";
        }
    }
} catch(Exception $e) {
    // En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
}