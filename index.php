<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8"/>
        <link href="css/base.css" rel="stylesheet"/>
        <link href="css/index.css" rel="stylesheet"/>
        <title>Les Happy hours</title>
    </head>

    <body>
        <header>
            <div class="head">
                <h1>
                    <strong><a href="index.php">Les Happy hours</a></strong>
                </h1>
                <p>Troupe d'improvisation théâtrale de Grenoble</p>
            </div>
            <div class="picture">
                <p>
                    <img alt="Photo de la troupe en représentation" src="images/header.jpg" />
                </p>
            </div>
        </header>

        <nav>
            <ul>
                <li><a href="index.php">Accueil</a></li>
                <li><a href="improvisation_troupe.php">La troupe</a></li>
                <li><a href="our_shows.php">Nos spectacles</a></li>
                <li><a href="calendar.php">Calendrier</a></li>
                <li><a href="booking.php">Réservations</a></li>
                <li><a href="contact.php">Contact</a></li>
                <li><a href="admin/admin.php">administrateur</a></li>
            </ul>
        </nav>

        <section>
            <div class="recherche_voyage">
                <h1>Nos dates
                    <a href="calendar.php"><br>
                        <img alt="Photo de la troupe en réunion" src="images/accueil.jpg" />
                    </a>
                </h1>
            </div>
            <div class="happy">
                <h1>Les Happy hours
                    <a href="improvisation_troupe.php"><br>
                        <img alt="Photo de la troupe en spectacle" src="images/header1.jpg" />
                    </a>
                </h1>
            </div>
            <div class="freeSeat">
                <h1>Prenez vos réservations
                    <a href="booking.php"><br>
                        <img alt="Photo de la troupe en scène" src="images/accueil2.jpg" />
                    </a>
                </h1>
            </div>
        </section>

        <footer>
            <h5> Les Happy hours, troupe d'improvisation</h5>
            <div class="bottomFooter">
                <a href="index.php">Accueil</a> /
                <a href="improvisation_troupe.php">La troupe</a> /
                <a href="our_shows.php">Nos spectacles</a> /
                <a href="calendar.php">Calendrier</a> /
                <a href="booking.php">Réservations</a> /
                <a href="contact.php">Contact</a> /
                <a href="admin/admin.php">Administrateur</a>
            </div>
        </footer>
    </body>
</html>